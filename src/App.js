import React from 'react';
import './App.css';
import CharacterList from './components/CharacterList/CharacterList';
import Character from './components/Character/Character';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        
        <Link to="/">
          <h3>Home</h3>
        </Link>
        <Switch>
          <Route exact path="/" component={CharacterList} />
          <Route path="/character/:id" component={Character}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
