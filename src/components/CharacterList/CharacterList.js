import React from "react";
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-grid-system';

const API_URL = "https://rickandmortyapi.com/api/character/";

class CharacterList extends React.Component {
  state = {
    characters: [],
    filtered: []
  };
  /*
  TODO: 
    Add all char names to filtered list
    Implement search bar in render
    Use filter to filter filtered list... for filtering
  */
  async componentDidMount() {
    try {
      const response = await fetch(API_URL).then(resp => resp.json());
      let chars = [...this.state.characters];
      chars.push(...response.results);

      this.setState({
        characters: chars,
        filtered: chars
      });
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const createGrid = () => {
      let grid = [];
      let children = [];

      for(let i = 0; i < this.state.filtered.length; i++){
        children.push(
          <Link to={`/character/${this.state.filtered[i].id}`} key={this.state.filtered[i].id} >
            <Col className="container" fluid="true" xs="content">
              <div className="card" ui-grid="gridOptions" ui-grid-auto-fit-columns="true">
                <img src={this.state.filtered[i].image} alt={this.state.filtered[i].name} />
                <h4 align="center">{this.state.filtered[i].name}</h4>
              </div>
            </Col>
          </Link>);

        //3 characters per row, with remainders at last row 
        if(i%3 === 2){
          grid.push(<Row fluid="true" style={{paddingTop: "10px"}}>{children}</Row>);
          children = [];
        } 
      }

      grid.push(<Row fluid="true">{children}</Row>);
      return grid;
    };

    return (
      <div style={{ margin:"0 auto" }}>
        <div>
          <nav>
            <input type="text" onChange={this.handleChange.bind(this)} className="input" id="searchBar" />
          </nav>
        </div>
        <Container>
            {createGrid()}
        </Container>
      </div>
    );
  }

  handleChange(e){
    let currentList = [];
    let newList = [];

    if (e.target.value !== "") {
      currentList = this.state.characters;

      newList = currentList.filter(char => {
        const searchTerm = char.name.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return searchTerm.includes(filter);
      });
    } else {
      newList = this.state.characters;
    }
    this.setState({
      filtered: newList
    });
  }
}

export default CharacterList;
