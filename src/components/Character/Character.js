import React from "react";

const API_URL = "https://rickandmortyapi.com/api/character/";

class Character extends React.Component {
    state = {
        character: Object,
        origin: Object,
        location: Object,
        episodes: Array
    }
    async componentDidMount() {
        let id = new URLSearchParams(this.props.match.params).get("id");
        let url = API_URL + id;
        
        try {
            await fetch(url)
                .then(resp => resp.json())
                .then(resp => {
                    let char = resp;
                    console.log(typeof char.episode);
                    this.setState({
                        character: char,
                        origin: char.origin,
                        location: char.location,
                        episodes: char.episode
                    })
                });
        } catch (e) {
            console.error(e);
        }
    }
    render() {

        const eps = [];
        for (let i = 0; i < this.state.episodes.length; i++){
            eps.push(<li key={i}><a href={this.state.episodes[i]}>{this.state.episodes[i]}</a></li>);
        }

        return (
            <div style={{ margin:"0 auto" }}>
                <img src={this.state.character.image} alt={this.state.character.name} />
                <h3>{this.state.character.name}</h3>
                <ul style={{ listStyleType: "none", textAlign:"left" }}>
                    <li>
                        Status: {this.state.character.status}
                    </li>
                    <li>
                        Species: {this.state.character.species}
                    </li>
                    <li>
                        Gender: {this.state.character.gender}
                    </li>
                    <li> 
                        Origin: <a href={this.state.origin.url}>{this.state.origin.name}</a> 
                    </li>
                    <li> 
                        Current location: <a href={this.state.location.url}>{this.state.location.name}</a> 
                    </li>
                </ul>
                <h5>Appears in episodes:</h5>
                <ol>
                    {eps}
                </ol>
            </div>
        );
    }
}

export default Character;